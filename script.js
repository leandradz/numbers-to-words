const unidade = ["","um","dois","três","quatro","cinco","seis","sete","oito","nove"];
const excessoes = ["dez","onze","doze","treze","catorze","quize","dezesseis","dezessete","dezoito","dezenove"];
const dezenas = ["","dez","vinte","trinta","quarenta","cinquenta","sessenta","setenta","oitenta","noventa"];
const centenas = ["","cento","duzentos","trezentos","quatrocentos","quinhentos","seiscentos","setecentos","oitocentos","novecentos"];
const milhares = ["", "mil"];

function numbersToWords() {
  let div = document.createElement('div')
  document.body.appendChild(div)
  let arrayResultado = []
  
  for (let n = 0; n < milhares.length; n++) {
    //se menor que mil:
    if (n === 0) {
      for (let m = 0; m < centenas.length; m++) {
        for (let l = 0; l < dezenas.length; l++) {
          
          if (l == 0 && m == 0) {
            /* Apenas 1 a 9 no inicio da contagem */
            for (let u = 0; u < unidade.length; u++) {
              if (u > 0) {
                arrayResultado.push(unidade[u])
              }
            }
          } else if (l == 1 && m == 0) {
            /* retorna 10 a 19 */
            for (let e = 0; e < excessoes.length; e++) {
              arrayResultado.push(excessoes[e])
            }
          } else if (l == 1) {
            for (let e = 0; e < excessoes.length; e++) {
              arrayResultado.push(`${centenas[m]} e ${excessoes[e]}`)
            }
          } else {
            //Contagem do 20 até 1000
            for (let j = 0; j < unidade.length; j++) {
              if (m == 0 && j == 0) {
                //imprimir apenas 'vinte, ' 'trinta, ' ...
                arrayResultado.push(dezenas[l])
              } else if (m == 0) {
                // imprimir menores que 100 
                arrayResultado.push(`${dezenas[l]} e ${unidade[j]}`)
              } else if (j == 0 && l == 0 && m == 1) {
                //excessão para a palavra 'cem'
                arrayResultado.push('cem')
              } else if (l == 0) {
                // centenas e unidades: 101, 102...
                arrayResultado.push(`${centenas[m]} e ${unidade[j]}`)
              } else if (j == 0) {
                // centenas e dezenas: 110, 120...
                arrayResultado.push(`${centenas[m]} e ${dezenas[l]}`)
              } else {
                // todos os outros numeros seguindo a sequencia acima.
                arrayResultado.push(`${milhares[n]} ${centenas[m]} e ${dezenas[l]} e ${unidade[j]}`)
              }
            } 
          } 
        }
      }
    }
    arrayResultado.push('mil')
    div.innerText = arrayResultado
    
    return arrayResultado
  }
}
numbersToWords();
